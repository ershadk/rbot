require 'socket'

module RbotHelper
  def reply(message)
    parsed_message = parse(message)
    if has_nick?(parsed_message)
      puts parsed_message
      @irc_socket.send_msg(parsed_message[:from_nick], parsed_message[:message])
    end
  end

  def has_nick?(parsed_message)
    parsed_message[:to_nick] == @irc_socket.nick
  end

  def parse(message)
    parsed = message.split(':')[2..-1]
    { from_nick: message.split('!').first[1..-1], to_nick: parsed.first, message: (parsed[1..-1] || []).join(':').strip }
  end

  def nick_mentioned
    true
  end
end

class IRCSocket
  attr_accessor :nick

  def initialize(args = {})
    @host     = args[:host]
    @port     = args[:port]    || 6667
    @nick     = args[:nick]    || 'EBot'
    @ident    = args[:ident]   || 'ebot'
    @realname = args[:ident]   || 'EBot'
    @channel  = args[:channel] || '#ershad_bot'

    @socket = TCPSocket.open @host, @port
  end

  def send(str)
    @socket.send "#{str}\r\n", 0
  end

  def connection
    @socket
  end

  def send_pong
    @socket.send('PONG', 0)
  end

  def connect
    self.send "NICK #{@nick}"
    self.send "USER #{@ident} #{@host} bla :%#{@realname}"
    self.send "JOIN #{@channel}"
    self.send_msg 'ershad', 'Hello World'
  end

  def send_msg(user, message)
    self.send "PRIVMSG #{@channel} :#{user}, #{message}"
  end
end

class Rbot
  include RbotHelper

  def initialize(args = {})
    @host     = args[:host]
    @port     = args[:port]
    @channel  = args[:channel]

    @irc_socket = IRCSocket.new host: @host, port: @port
  end

  def loop
    @irc_socket.connect

    while message = @irc_socket.connection.gets
      message == 'PING' ? @irc_socket.send_pong : @irc_socket.send(reply(message.chomp))
    end

    @irc_socket.connection.close
  end
end

Rbot.new(host: 'irc.freenode.net', port: 6667).loop